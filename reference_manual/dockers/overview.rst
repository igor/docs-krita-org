.. meta::
   :description:
        Overview of the overview docker.

.. metadata-placeholder

   :authors: - Scott Petrovic
             - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
   :license: GNU free documentation license 1.3 or later.

.. index:: Overview, Navigation
.. _overview_docker:

========
Overview
========

.. image:: /images/dockers/Krita_Overview_Docker.png

This docker allows you to see a full overview of your image. You can also use it to navigate and zoom in and out quickly. Dragging the view-rectangle allows you quickly move the view.

There are furthermore basic navigation functions: Dragging the zoom-slider allows you quickly change the zoom.

.. versionadded:: 4.2
    
    Toggling the mirror button will allow you to mirror the view of the canvas (but not the full image itself) and dragging the rotate slider allows you to adjust the rotation of the viewport. To reset the rotation, |mouseright| the slider to edit the number, and type '0'.
