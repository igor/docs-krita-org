# Translation of docs_krita_org_reference_manual___tools___gradient_draw.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-24 16:25+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Antialias threshold"
msgstr "Llindar antialiàsing"

#: ../../<rst_epilog>:58
msgid ""
".. image:: images/icons/gradient_drawing_tool.svg\n"
"   :alt: toolgradient"
msgstr ""
".. image:: images/icons/gradient_drawing_tool.svg\n"
"   :alt: eina de degradat"

#: ../../reference_manual/tools/gradient_draw.rst:1
msgid "Krita's gradient tool reference."
msgstr "Referència de l'eina Degradat del Krita."

#: ../../reference_manual/tools/gradient_draw.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/gradient_draw.rst:11
msgid "Gradient"
msgstr "Degradat"

#: ../../reference_manual/tools/gradient_draw.rst:16
msgid "Gradient Tool"
msgstr "Eina de degradat"

#: ../../reference_manual/tools/gradient_draw.rst:18
msgid "|toolgradient|"
msgstr "|toolgradient|"

#: ../../reference_manual/tools/gradient_draw.rst:20
msgid ""
"The Gradient tool is found in the Tools Panel. Left-Click dragging this tool "
"over the active portion of the canvas will draw out the current gradient.  "
"If there is an active selection then, similar to the :ref:`fill_tool`, the "
"paint action will be confined to the selection's borders."
msgstr ""
"L'eina de Degradat es troba al Plafó d'eines. Feu clic esquerre arrossegant "
"aquesta eina sobre la part activa del llenç per a dibuixar el degradat "
"actual. Si hi ha una selecció activa, similar a l':ref:`fill_tool`, l'acció "
"de pintar es limitarà a les vores de la selecció."

#: ../../reference_manual/tools/gradient_draw.rst:23
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/gradient_draw.rst:25
msgid "Shape:"
msgstr "Forma:"

#: ../../reference_manual/tools/gradient_draw.rst:27
msgid "Linear"
msgstr "Lineal"

#: ../../reference_manual/tools/gradient_draw.rst:28
msgid "This will draw the gradient straight."
msgstr "Dibuixarà el degradat recte."

#: ../../reference_manual/tools/gradient_draw.rst:29
msgid "Radial"
msgstr "Radial"

#: ../../reference_manual/tools/gradient_draw.rst:30
msgid ""
"This will draw the gradient from a center, defined by where you start the "
"stroke."
msgstr "Dibuixarà el degradat des d'un centre, definit per on comença el traç."

#: ../../reference_manual/tools/gradient_draw.rst:31
msgid "Square"
msgstr "Quadrat"

#: ../../reference_manual/tools/gradient_draw.rst:32
msgid ""
"This will draw the gradient from a center in a square shape, defined by "
"where you start the stroke."
msgstr ""
"Dibuixarà el degradat des d'un centre en una forma quadrada, definit per on "
"comença el traç."

#: ../../reference_manual/tools/gradient_draw.rst:33
msgid "Conical"
msgstr "Cònic"

#: ../../reference_manual/tools/gradient_draw.rst:34
msgid ""
"This will wrap the gradient around a center, defined by where you start the "
"stroke."
msgstr ""
"Envoltarà el degradat al voltant d'un centre, definit per on comença el traç."

#: ../../reference_manual/tools/gradient_draw.rst:35
msgid "Conical-symmetric"
msgstr "Cònic simètric"

#: ../../reference_manual/tools/gradient_draw.rst:36
msgid ""
"This will wrap the gradient around a center, defined by where you start the "
"stroke, but will mirror the wrap once."
msgstr ""
"Envoltarà el degradat al voltant d'un centre, definit per on comença el "
"traç, però reflectirà l'embolcall una vegada."

#: ../../reference_manual/tools/gradient_draw.rst:38
msgid "Shaped"
msgstr "Amb forma"

#: ../../reference_manual/tools/gradient_draw.rst:38
msgid "This will shape the gradient depending on the selection or layer."
msgstr "Donarà forma al degradat en funció de la selecció o capa."

#: ../../reference_manual/tools/gradient_draw.rst:40
msgid "Repeat:"
msgstr "Repetició:"

#: ../../reference_manual/tools/gradient_draw.rst:42
msgid "None"
msgstr "Cap"

#: ../../reference_manual/tools/gradient_draw.rst:43
msgid "This will extend the gradient into infinity."
msgstr "Estendrà el degradat fins a l'infinit."

#: ../../reference_manual/tools/gradient_draw.rst:44
msgid "Forward"
msgstr "Endavant"

#: ../../reference_manual/tools/gradient_draw.rst:45
msgid "This will repeat the gradient into one direction."
msgstr "Repetirà el degradat en una direcció."

#: ../../reference_manual/tools/gradient_draw.rst:47
msgid "Alternating"
msgstr "Alternat"

#: ../../reference_manual/tools/gradient_draw.rst:47
msgid ""
"This will repeat the gradient, alternating the normal direction and the "
"reversed."
msgstr "Repetirà el degradat, alternant la direcció normal i la inversa."

#: ../../reference_manual/tools/gradient_draw.rst:49
msgid "Reverse"
msgstr "Inverteix"

#: ../../reference_manual/tools/gradient_draw.rst:50
msgid "Reverses the direction of the gradient."
msgstr "Inverteix la direcció del degradat."

#: ../../reference_manual/tools/gradient_draw.rst:52
msgid "Doesn't do anything, original function must have gotten lost in a port."
msgstr ""
"No fa res, la funció original es deu haver perdut durant una adaptació."
