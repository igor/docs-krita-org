# Translation of docs_krita_org_reference_manual___layers_and_masks___clone_layers.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 18:44+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:1
msgid "How to use clone layers."
msgstr "Com emprar les capes clonades."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Layers"
msgstr "Capes"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Linked Clone"
msgstr "Clonada vinculada"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Clone Layer"
msgstr "Capa clonada"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:20
msgid "Clone Layers"
msgstr "Capes clonades"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:22
msgid ""
"A clone layer is a layer that keeps an up-to-date copy of another layer. You "
"cannot draw or paint on it directly, but it can be used to create effects by "
"applying different types of layers and masks (e.g. filter layers or masks)."
msgstr ""
"Una capa clonada és una capa que manté una còpia actualitzada d'una altra "
"capa. No podreu dibuixar o pintar directament sobre seu, però es podrà "
"utilitzar per a crear efectes aplicant diferents tipus de capes i màscares "
"(p. ex., capes o màscares de filtratge)."

# skip-rule: punctuation-period
#: ../../reference_manual/layers_and_masks/clone_layers.rst:25
msgid "Example uses of Clone Layers."
msgstr "Exemple que empra les capes clonades"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:27
msgid ""
"For example, if you were painting a picture of some magic person and wanted "
"to create a glow around them that was updated as you updated your character, "
"you could:"
msgstr ""
"Per exemple, si esteu pintant la imatge d'alguna persona màgica i voleu "
"crear una lluïssor al voltant seu que s'actualitzi a mesura que actualitzeu "
"el vostre personatge, podríeu:"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:29
msgid "Have a Paint Layer where you draw your character"
msgstr "Tenir una capa de pintura on dibuixareu el vostre personatge."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:30
msgid ""
"Use the Clone Layer feature to create a clone of the layer that you drew "
"your character on"
msgstr ""
"Utilitzar la característica Capa clonada per a crear un clon de la capa on "
"heu dibuixat el vostre personatge."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:31
msgid ""
"Apply an HSV filter mask to the clone layer to make the shapes on it white "
"(or blue, or green etc.)"
msgstr ""
"Aplicar una màscara de filtratge HSV a la capa clonada per a fer que les "
"formes siguin blanques (blaves o verdes, etc.)."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:32
msgid "Apply a blur filter mask to the clone layer so it looks like a \"glow\""
msgstr ""
"Aplicar una màscara de filtratge amb difuminat a la capa clonada, de manera "
"que sembli una «lluïssor»."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:34
msgid ""
"As you keep painting and adding details, erasing on the first layer, Krita "
"will automatically update the clone layer, making your \"glow\" apply to "
"every change you make."
msgstr ""
"Mentre seguiu pintant i afegint detalls, esborrant sobre la primera capa, el "
"Krita actualitzarà automàticament la capa clonada, fent que la «lluïssor» "
"s'apliqui a cada canvi que feu."
