# Translation of docs_krita_org_reference_manual___dockers___onion_skin.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 20:32+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/dockers/onion_skin.rst:1
msgid "Overview of the onion skin docker."
msgstr "Vista general de l'acoblador Pell de ceba."

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Animation"
msgstr "Animació"

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Onion Skin"
msgstr "Pell de ceba"

#: ../../reference_manual/dockers/onion_skin.rst:15
msgid "Onion Skin Docker"
msgstr "Acoblador Pell de ceba"

#: ../../reference_manual/dockers/onion_skin.rst:18
msgid ".. image:: images/dockers/Onion_skin_docker.png"
msgstr ".. image:: images/dockers/Onion_skin_docker.png"

#: ../../reference_manual/dockers/onion_skin.rst:19
msgid ""
"To make animation easier, it helps to see both the next frame as well as the "
"previous frame sort of layered on top of the current. This is called *onion-"
"skinning*."
msgstr ""
"Per a facilitar l'animació, ajuda veure tant el fotograma següent com "
"l'anterior superposat en capes a sobre de l'actual. Això s'anomena *pell de "
"ceba*."

#: ../../reference_manual/dockers/onion_skin.rst:22
msgid ".. image:: images/dockers/Onion_skin_01.png"
msgstr ".. image:: images/dockers/Onion_skin_01.png"

#: ../../reference_manual/dockers/onion_skin.rst:23
msgid ""
"Basically, they are images that represent the frames before and after the "
"current frame, usually colored or tinted."
msgstr ""
"Bàsicament, són imatges que representen els fotogrames abans i després del "
"fotograma actual, generalment en color o en tinta."

#: ../../reference_manual/dockers/onion_skin.rst:25
msgid ""
"You can toggle them by clicking the lightbulb icon on a layer that is "
"animated (so, has frames), and isn’t fully opaque. (Krita will consider "
"white to be white, not transparent, so don’t animated on an opaque layer if "
"you want onion skins.)"
msgstr ""
"Podreu alternar fent clic a la icona de la bombeta sobre una capa que està "
"animada (per tant, conté fotogrames) i que no és completament opaca. (El "
"Krita considera que el blanc és blanc, no transparent, així que no animeu "
"sobre una capa opaca si voleu les pells de ceba)."

#: ../../reference_manual/dockers/onion_skin.rst:29
msgid ""
"Since 4.2 onion skins are disabled on layers whose default pixel is fully "
"opaque. These layers can currently only be created by using :guilabel:"
"`background as raster layer` in the :guilabel:`content` section of the new "
"image dialog. Just don't try to animate on a layer like this if you rely on "
"onion skins, instead make a new one."
msgstr ""
"Des de la versió 4.2, les pells de ceba estan inhabilitades sobre les capes "
"on el píxel predeterminat és totalment opac. Actualment, aquestes capes "
"només es poden crear utilitzant el :guilabel:`Fons com a capa ràster` a la "
"secció :guilabel:`Contingut` del diàleg d'imatge nova. Simplement no "
"intenteu animar sobre una capa com aquesta si confieu en les pells de ceba, "
"en lloc d'això, creeu-ne una de nova."

#: ../../reference_manual/dockers/onion_skin.rst:31
msgid ""
"The term onionskin comes from the fact that onions are semi-transparent. In "
"traditional animation animators would make their initial animations on "
"semitransparent paper on top of an light-table (of the special animators "
"variety), and they’d start with so called keyframes, and then draw frames in "
"between. For that, they would place said keyframes below the frame they were "
"working on, and the light table would make the lines of the keyframes shine "
"through, so they could reference them."
msgstr ""
"El terme pell de ceba prové del fet que les cebes són semitransparents. En "
"l'animació tradicional, els animadors realitzarien les seves animacions "
"inicials en paper semitransparent sobre una taula de llum (de la varietat "
"d'animadors especials), i començarien amb els anomenats fotogrames clau, i "
"després dibuixarien els fotogrames intermedis. Per això, col·locarien "
"aquests fotogrames clau sota del marc en què estaven treballant, i la taula "
"de llum faria que brillessin les línies dels fotogrames clau, de manera que "
"les podien emprar com a referència."

#: ../../reference_manual/dockers/onion_skin.rst:33
msgid ""
"Onion-skinning is a digital implementation of such a workflow, and it’s very "
"useful when trying to animate."
msgstr ""
"La pell de ceba és una implementació digital d'aquest flux de treball, i és "
"molt útil quan es tracta d'animar."

#: ../../reference_manual/dockers/onion_skin.rst:36
msgid ".. image:: images/dockers/Onion_skin_02.png"
msgstr ".. image:: images/dockers/Onion_skin_02.png"

#: ../../reference_manual/dockers/onion_skin.rst:37
msgid ""
"The slider and the button with zero offset control the master opacity and "
"visibility of all the onion skins. The boxes at the top allow you to toggle "
"them on and off quickly, the main slider in the middle is a sort of ‘master "
"transparency’ while the sliders to the side allow you to control the "
"transparency per keyframe offset."
msgstr ""
"El control lliscant i el botó amb desplaçament de zero controlen l'opacitat "
"principal i la visibilitat de totes les pells de ceba. Els quadres de la "
"part superior permeten activar-los i desactivar-los ràpidament, el control "
"lliscant principal que està en el mig és una espècie de «transparència "
"mestra», mentre que els controls lliscants que hi ha als costats permeten "
"controlar la transparència per al desplaçament del fotograma clau."

#: ../../reference_manual/dockers/onion_skin.rst:39
msgid ""
"Tint controls how strongly the frames are tinted, the first screen has 100%, "
"which creates a silhouette, while below you can still see a bit of the "
"original colors at 50%."
msgstr ""
"El tint controla la intensitat amb què s'aplica tinta als fotogrames, la "
"primera pantalla té un 100%, el qual crea una silueta, mentre que a sota "
"encara podreu veure una mica dels colors originals a un 50%."

#: ../../reference_manual/dockers/onion_skin.rst:41
msgid ""
"The :guilabel:`Previous Frame` and :guilabel:`Next Frame` color labels "
"allows you set the colors."
msgstr ""
"Els rètols de color :guilabel:`Fotograma anterior` i :guilabel:`Fotograma "
"següent` permeten establir els colors."
