# Translation of docs_krita_org_reference_manual___dockers___animation_curve.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-08-24 16:39+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../reference_manual/dockers/animation_curve.rst:1
msgid "Overview of the animation curves docker."
msgstr "Vista general de l'acoblador Corbes d'animació."

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Animation"
msgstr "Animació"

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Animation Curves"
msgstr "Corbes d'animació"

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Interpolation"
msgstr "Interpolació"

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Tweening"
msgstr "Interpolació"

#: ../../reference_manual/dockers/animation_curve.rst:15
msgid "Animation Curves Docker"
msgstr "Acoblador Corbes d'animació"

#: ../../reference_manual/dockers/animation_curve.rst:17
msgid ""
"The Animation Curve docker allows you to edit tweened sections by means of "
"interpolation curves. As of this time of writing, it can only edit opacity."
msgstr ""
"L'acoblador Corbes d'animació permet editar seccions interpolades mitjançant "
"corbes d'interpolació. A partir d'aquest moment de l'escriptura, només "
"podreu editar l'opacitat."

#: ../../reference_manual/dockers/animation_curve.rst:19
msgid ""
"The idea is that sometimes what you want to animate can be expressed as a "
"value. This allows the computer to do maths on the values, and automate "
"tasks, like interpolation, also known as 'Tweening'. Because these are "
"values, like percentage opacity, and animation happens over time, that means "
"we can visualize the way the values are interpolated as a curve graph, and "
"also edit the graph that way."
msgstr ""
"La idea és que de vegades el que voleu animar es pot expressar com un valor. "
"Permetrà a l'ordinador fer càlculs matemàtics sobre els valors i "
"automatitzar les tasques, com la interpolació. Com que aquests són valors, "
"com el percentatge d'opacitat i l'animació que passa al llarg del temps, "
"això vol dir que podrem visualitzar com un gràfic de corbes la forma en què "
"s'interpolaran els valors, i també editar el gràfic de la manera següent."

#: ../../reference_manual/dockers/animation_curve.rst:21
msgid ""
"But, when you first open this docker, there's no curves visible! You will "
"first need to add opacity keyframes to the active animation layer. You can "
"do this by using the animation docker and selection :guilabel:`Add new "
"keyframe`."
msgstr ""
"Però, quan obriu per primera vegada aquest acoblador, no hi ha cap corba "
"visible! Primer haureu d'afegir fotogrames clau d'opacitat a la capa "
"d'animació activa. Podreu fer-ho emprant l'acoblador Animació i "
"seleccionant :guilabel:`Afegeix un fotograma clau nou`."

#: ../../reference_manual/dockers/animation_curve.rst:25
msgid ".. image:: images/dockers/Animation_curves_1.png"
msgstr ".. image:: images/dockers/Animation_curves_1.png"

#: ../../reference_manual/dockers/animation_curve.rst:26
msgid ""
"Opacity should create a bright red curve line in the docker. On the left, in "
"the layer list, you will see that the active layer has an outline of its "
"properties: A red :guilabel:`Opacity` has appeared. Pressing the red dot "
"will hide the current curve, which'll be more useful in the future when more "
"properties can be animated."
msgstr ""
"L'opacitat haurà de crear una línia de corba vermella brillant a "
"l'acoblador. A l'esquerra, a la llista de capes, veureu que la capa activa "
"té un contorn de les seves propietats: ha aparegut una :guilabel:`Opacitat` "
"en vermell. En pressionar el punt vermell s'ocultarà la corba actual, la "
"qual serà més útil en el futur quan es puguin animar més propietats."

#: ../../reference_manual/dockers/animation_curve.rst:29
msgid ".. image:: images/dockers/Animation_curves_2.png"
msgstr ".. image:: images/dockers/Animation_curves_2.png"

#: ../../reference_manual/dockers/animation_curve.rst:30
msgid ""
"If you select a dot of the curve, you can move it around to shift its place "
"in the time-line or its value."
msgstr ""
"Si seleccioneu un punt de la corba, podreu moure-la per a canviar el seu "
"lloc en la línia de temps o el seu valor."

#: ../../reference_manual/dockers/animation_curve.rst:32
msgid "On the top, you can select the method of smoothing:"
msgstr "A la part superior, podreu seleccionar el mètode de suavitzat:"

#: ../../reference_manual/dockers/animation_curve.rst:34
msgid "Hold Value"
msgstr "Mantén el valor"

# skip-rule: barb-igual
#: ../../reference_manual/dockers/animation_curve.rst:35
msgid "This keeps the value the same until there's a new keyframe."
msgstr "Això mantindrà el valor igual fins que hi hagi un fotograma clau nou."

#: ../../reference_manual/dockers/animation_curve.rst:36
msgid "Linear Interpolation (Default)"
msgstr "Interpolació lineal (predeterminat)"

#: ../../reference_manual/dockers/animation_curve.rst:37
msgid "This gives a straight interpolation between two values."
msgstr "Això donarà una interpolació directa entre dos valors."

#: ../../reference_manual/dockers/animation_curve.rst:39
msgid "Custom interpolation"
msgstr "Interpolació personalitzada"

#: ../../reference_manual/dockers/animation_curve.rst:39
msgid ""
"This allows you to set the section after the keyframe node as one that can "
"be modified. |mouseleft| +dragging on the node allows you to drag out a "
"handler node for adjusting the curving."
msgstr ""
"Permetrà establir la secció després del node del fotograma clau com una que "
"es pot modificar. :kbd:`Fer` |mouseleft| :kbd:`+ arrossegar` sobre el node "
"permetrà arrossegar una nansa del node per ajustar la corba."

#: ../../reference_manual/dockers/animation_curve.rst:41
msgid ""
"So, for example, making a 100% opacity keyframe on frame 0 and a 0% opacity "
"one on frame 24 gives the following result:"
msgstr ""
"Així, per exemple, crear un fotograma clau amb l'opacitat del 100% en el "
"fotograma 0 i una opacitat del 0% en el fotograma 24 donarà el següent "
"resultat:"

#: ../../reference_manual/dockers/animation_curve.rst:44
msgid ".. image:: images/dockers/Ghost_linear.gif"
msgstr ".. image:: images/dockers/Ghost_linear.gif"

#: ../../reference_manual/dockers/animation_curve.rst:45
msgid ""
"If we select frame 12 and press :guilabel:`Add New Keyframe` a new opacity "
"keyframe will be added on that spot. We can set this frame to 100% and set "
"frame 0 to 0% for this effect."
msgstr ""
"Si seleccionem el fotograma 12 i premem :guilabel:`Afegeix un fotograma clau "
"nou`, s'afegirà un fotograma clau nou d'opacitat en aquest lloc. Podrem "
"establir aquest fotograma al 100% i establir el fotograma 0 al 0% per a "
"aquest efecte."

#: ../../reference_manual/dockers/animation_curve.rst:48
msgid ".. image:: images/dockers/Ghost_linear_in-out.gif"
msgstr ".. image:: images/dockers/Ghost_linear_in-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:49
msgid ""
"Now, if we want easing in, we select the node on frame 0 and press the :"
"guilabel:`Custom Interpolation` button at the top. This will enable custom "
"interpolation on the curve between frames 0 and 12. Doing the same on frame "
"12 will enable custom interpolation between frames 12 and 24. Drag from the "
"node to add a handle, which in turn you can use to get the following effects:"
msgstr ""
"Ara, si volem alleujar, seleccionarem el node en el fotograma 0 i premerem "
"el botó :guilabel:`Interpolació personalitzada` que hi ha a la part "
"superior. Permetrà la interpolació personalitzada sobre la corba entre els "
"fotogrames 0 i 12. En fer el mateix sobre el fotograma 12, s'habilitarà la "
"interpolació personalitzada entre els quadres 12 i 24. Arrossegueu des del "
"node per afegir una nansa, la qual al seu torn es podrà utilitzar per "
"obtenir els següents efectes:"

#: ../../reference_manual/dockers/animation_curve.rst:52
msgid ".. image:: images/dockers/Ghost_ease_in-out.gif"
msgstr ".. image:: images/dockers/Ghost_ease_in-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:54
msgid ".. image:: images/dockers/Animation_curves_3.png"
msgstr ".. image:: images/dockers/Animation_curves_3.png"

#: ../../reference_manual/dockers/animation_curve.rst:55
msgid "The above shows an ease-in curve."
msgstr "Tot l'anterior mostrarà una corba amb augment accelerat."

#: ../../reference_manual/dockers/animation_curve.rst:57
msgid "And convex/concave examples:"
msgstr "I els exemples convexos/còncaus:"

#: ../../reference_manual/dockers/animation_curve.rst:60
msgid ".. image:: images/dockers/Ghost_concave_in-out.gif"
msgstr ".. image:: images/dockers/Ghost_concave_in-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:62
msgid ".. image:: images/dockers/Animation_curves_4.png"
msgstr ".. image:: images/dockers/Animation_curves_4.png"

#: ../../reference_manual/dockers/animation_curve.rst:64
msgid ".. image:: images/dockers/Ghost_convex_int-out.gif"
msgstr ".. image:: images/dockers/Ghost_convex_int-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:66
msgid ".. image:: images/dockers/Animation_curves_5.png"
msgstr ".. image:: images/dockers/Animation_curves_5.png"

#: ../../reference_manual/dockers/animation_curve.rst:67
msgid ""
"As you may be able to tell, there's quite a different 'texture', so to "
"speak, to each of these animations, despite the difference being only in the "
"curves. Indeed, a good animator can get quite some tricks out of "
"interpolation curves, and as we develop Krita, we hope to add more "
"properties for you to animate this way."
msgstr ""
"Com podeu veure, hi ha una «textura» força diferent, per dir-ho d'aquesta "
"manera, en cadascuna d'aquestes animacions, tot i que la diferència està "
"només en les corbes. De fet, un bon animador pot obtenir alguns trucs de les "
"corbes d'interpolació, i a mesura que desenvolupem Krita, esperem afegir més "
"propietats perquè puguis animar d'aquesta manera."

#: ../../reference_manual/dockers/animation_curve.rst:71
msgid ""
"Opacity has currently 255 as maximum in the curve editor, as that's how "
"opacity is stored internally."
msgstr ""
"L'opacitat actualment té 255 com a màxim a l'editor de corbes, ja que així "
"és com l'opacitat s'emmagatzema de manera interna."
