# Translation of docs_krita_org_contributors_manual___community.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_contributors_manual___community\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:08+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../contributors_manual/community.rst:1
msgid "Guide to the Krita community"
msgstr "Настанови щодо спільноти Krita"

#: ../../contributors_manual/community.rst:10
msgid "community"
msgstr "спільнота"

#: ../../contributors_manual/community.rst:10
msgid "communication"
msgstr "спілкування"

#: ../../contributors_manual/community.rst:16
msgid "The Krita Community"
msgstr "Спільнота Krita"

#: ../../contributors_manual/community.rst:18
msgid ""
"Get in touch! Apart from the website at https://www.krita.org, the Krita "
"project has three main communication channels:"
msgstr ""
"Виходьте на зв'язок! Окрім сайта за адресою https://www.krita.org, у проєкту "
"Krita є три основних канали обміну інформацією:"

#: ../../contributors_manual/community.rst:20
msgid "Internet Relay Chat (IRC)"
msgstr "Internet Relay Chat (IRC)"

#: ../../contributors_manual/community.rst:21
msgid "The mailing list"
msgstr "Список листування"

#: ../../contributors_manual/community.rst:22
#: ../../contributors_manual/community.rst:86
msgid "Phabricator"
msgstr "Phabricator"

#: ../../contributors_manual/community.rst:24
msgid ""
"While Krita developers and users are present on social media such as "
"Twitter, Mastodon, Reddit, Google+, Tumblr or Facebook, those are not the "
"place where we discuss new features, bugs, development or where we make "
"plans for the future."
msgstr ""
"Хоча у розробників і користувачів Krita є сторінки у соціальних мережах, "
"зокрема у Twitter, Mastodon, Reddit, Google+, Tumblr та Facebook, це не "
"місце, де ми обговорюємо нові можливості, вади, розробку або плани на "
"майбутнє."

#: ../../contributors_manual/community.rst:26
msgid "There are also the:"
msgstr "Крім того, ми маємо такі ресурси:"

#: ../../contributors_manual/community.rst:28
msgid "bug tracker"
msgstr "система стеження за вадами"

#: ../../contributors_manual/community.rst:29
msgid "development sprints"
msgstr "спринти розробників"

#: ../../contributors_manual/community.rst:31
msgid ""
"You’ll find that there are a number of people are almost always around: the "
"core team."
msgstr "Декілька людей слідкують майже за усім — це основа команди."

#: ../../contributors_manual/community.rst:33
msgid ""
"Boudewijn (irc: boud): project maintainer, lead developer. Works full-time "
"on Krita. Manages the Krita Foundation, triages bugs, does social media and "
"admin stuff. Boudewijn is also on Reddit as boudewijnrempt."
msgstr ""
"Боудевейн (irc: boud): супровідник проєкту, провідний розробник. Присвячує "
"Krita увесь свій робочий час. Керує Krita Foundation, визначає причини вад, "
"керує розповсюдженням новин у соціальних мережах та адмініструє сайти. "
"Боудевейн є користувачем Reddit із псевдонімом boudewijnrempt."

#: ../../contributors_manual/community.rst:34
msgid "Dmitry (irc: dmitryk_log): lead developer. Works full-time on Krita."
msgstr ""
"Дмітрій (irc: dmitryk_log): провідний розробник. Працює над Krita повний "
"робочий день."

#: ../../contributors_manual/community.rst:35
msgid ""
"Wolthera (irc: wolthera_laptop): developer, writes the manual and tutorials, "
"triages bugs, helps people out"
msgstr ""
"Вольтера (irc: wolthera_laptop): розробник, автор підручника і настанов, "
"вивчення вад, допомога людям."

#: ../../contributors_manual/community.rst:36
msgid "Scott Petrovic (irc: scottyp): UX designer, developer, webmaster"
msgstr ""
"Скот Петровіч (irc: scottyp): розробник інтерфейсу, розробник коду, "
"вебмайстер"

#: ../../contributors_manual/community.rst:37
msgid ""
"David Revoy (irc: deevad): expert user, creates Pepper & Carrot, maintains "
"the preset bundle."
msgstr ""
"Давід Ревой (irc: deevad): досвідчений користувач, творець Pepper & Carrot, "
"супровідник набору шаблонів."

#: ../../contributors_manual/community.rst:38
msgid "Alvin Wong (irc: windragon): windows guru"
msgstr "Алвін Вонг (irc: windragon): знавець windows"

#: ../../contributors_manual/community.rst:39
msgid "Ben Cooksley (irc: bcooksley): KDE system administrator."
msgstr "Ben Cooksley (irc: bcooksley): системний адміністратор KDE."

#: ../../contributors_manual/community.rst:41
msgid ""
"Krita’s team spans the globe, but most development happens in Europe and "
"Russia."
msgstr ""
"Розробники Krita живуть на усій земній кулі, але більшість з них мешкає у "
"Європі та Росії."

#: ../../contributors_manual/community.rst:43
msgid ""
"Krita is part of the larger KDE community. The KDE® Community is a free "
"software community dedicated to creating an open and user-friendly computing "
"experience, offering an advanced graphical desktop, a wide variety of "
"applications for communication, work, education and entertainment and a "
"platform to easily build new applications upon. The KDE contributors guide "
"is relevant for Krita contributors, too, and can be found `here <https://"
"archive.flossmanuals.net/kde-guide/>`_."
msgstr ""
"Krita є частиною великої спільноти KDE. Спільнота KDE® є спільнотою з "
"розвитку програмного забезпечення, метою якої є створення відритого та "
"дружнього до користувачів комп'ютерного середовища. Спільнота працює над "
"графічним робочим середовищем із широкими можливостями, широкими діапазоном "
"програми для спілкування, роботи, освіти та розваг, а також платформою для "
"побудови нових програм. Настанови для учасників спільноти KDE стосуються і "
"учасників розробки Krita. Ознайомитися із цими настановами можна `тут "
"<https://archive.flossmanuals.net/kde-guide/>`_."

#: ../../contributors_manual/community.rst:45
msgid ""
"The Krita Foundation was created to support development of Krita. The Krita "
"Foundation has sponsored Dmitry’s work on Krita since 2013."
msgstr ""
"Krita Foundation було створено для підтримки розробки Krita. Krita "
"Foundation спонсорує роботу Дмітрія над Krita з 2013 року."

#: ../../contributors_manual/community.rst:48
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#: ../../contributors_manual/community.rst:50
msgid ""
"IRC is the main communication channel. There are IRC clients for every "
"operating system out there, as well as a web client on the krita website."
msgstr ""
"Основним каналом спілкування є IRC. Клієнтські програми IRC є для майже будь-"
"якої операційної системи. Крім того, можна скористатися вебклієнтом на сайті "
"krita."

#: ../../contributors_manual/community.rst:52
msgid ""
"Joining IRC: connect to irc.freenode.net, select a unique nickname and join "
"the #krita and ##krita-chat channels. #krita is for on-topic talk, ##krita-"
"chat for off-topic chat."
msgstr ""
"Долучення до IRC: з'єднайтеся із irc.freenode.net, виберіть унікальний "
"псевдонім і долучайтеся до каналів #krita та ##krita-chat. Канал #krita "
"призначено для обговорення програми, канал ##krita-chat — для обговорення "
"інших тем."

#: ../../contributors_manual/community.rst:53
msgid "Don’t ask to ask: if you’ve got a question, just ask it."
msgstr "Не питайте, чи можна спитати: маєте питання — задайте його."

#: ../../contributors_manual/community.rst:54
msgid ""
"Don’t panic if several discussions happen at the same time. That’s normal in "
"a busy channel."
msgstr ""
"Не панікуйте, якщо одразу відбувається декілька обговорень. Це звичайна "
"практика для зайнятих каналів."

#: ../../contributors_manual/community.rst:55
msgid "Talk to an individual by typing their nick and a colon."
msgstr ""
"Поспілкуватися із кимось окремо можна додавши перед повідомленням його чи її "
"псевдонім із додаванням двокрапки."

#: ../../contributors_manual/community.rst:56
msgid ""
"Almost every Monday, at 14:00 CET or CEST, we have a meeting where we "
"discuss what happened in the past week, what we’re doing, and everything "
"that’s relevant for the project. The meeting notes are kept in google docs."
msgstr ""
"Майже кожного понеділка о 14:00 CET або CEST, ми проводимо зустріч, де "
"обговорюємо події минулого тижня, поточну роботу та усе, що стосується "
"проєкту. Протоколи зустрічей зберігаються у Документах google."

#: ../../contributors_manual/community.rst:57
msgid ""
"Activity is highest in CET or CEST daytime and evenings. US daytime and "
"evenings are most quiet."
msgstr ""
"Найвищий рівень активності спостерігається у денний та вечірній час за CET "
"або CEST. Денний та вечірній час за поясами США є часом найменшої активності."

#: ../../contributors_manual/community.rst:58
msgid ""
"**IRC is not logged. If you close the channel, you will be gone, and you "
"will not be able to read what happened when you join the channel again. If "
"you ask a question, you have to stay around!**"
msgstr ""
"**Журнал повідомлень у IRC не ведеться. Якщо ви закриєте вкладку каналу, ви "
"полишите його і не зможете прочитати повідомлення, які було надіслано "
"протягом часу вашої відсутності. Якщо задали питання, маєте дочекатися "
"відповіді!**"

#: ../../contributors_manual/community.rst:59
msgid ""
"It is really irritating for other users and disrupting to conversations if "
"you keep connecting and disconnecting."
msgstr ""
"Якщо ви постійно встановлюватимете з'єднання із каналом і розриватимете "
"його, це дратуватиме інших користувачів і заважатиме логічній послідовності "
"спілкування."

#: ../../contributors_manual/community.rst:63
msgid "Mailing List"
msgstr "Список листування"

#: ../../contributors_manual/community.rst:65
msgid ""
"The mailing list is used for announcements and sparingly for discussions. "
"Everyone who wants to work on Krita one way or another should be subscribed "
"to the mailing list."
msgstr ""
"Список листування використовується для оголошень та, іноді, для обговорень. "
"Усі, хто хоче працювати над Krita так чи інакше, мають підписатися на список "
"листування."

#: ../../contributors_manual/community.rst:67
msgid ""
"`Mailing List Archives <https://mail.kde.org/mailman/listinfo/kimageshop>`_"
msgstr ""
"`Архів списку листування <https://mail.kde.org/mailman/listinfo/kimageshop>`_"

#: ../../contributors_manual/community.rst:69
msgid ""
"The mailing list is called \"kimageshop\", because that is the name under "
"which the Krita project was started. Legal issues (surprise!) led to two "
"renames, once to Krayon, then to Krita."
msgstr ""
"Список листування має назву «kimageshop», оскільки це назва, яку було "
"вибрано на початку для проєкту Krita. Проблеми із авторським правом (яка "
"несподіванка!) призвели до двох перейменувань — спочатку на Krayon, а потім "
"на Krita."

#: ../../contributors_manual/community.rst:73
msgid "Gitlab (KDE Invent)"
msgstr "Gitlab (KDE Invent)"

#: ../../contributors_manual/community.rst:75
msgid "Gitlab serves the following purposes for the Krita team:"
msgstr "Gitlab у команді Krita виконує такі ролі:"

#: ../../contributors_manual/community.rst:77
msgid ""
"Review volunteers' submissions: https://invent.kde.org/kde/krita/"
"merge_requests for the code itself, https://invent.kde.org/websites/docs-"
"krita-org/merge_requests for the content of the Krita Manual."
msgstr ""
"Рецензування поданих іншими користувачами змін: https://invent.kde.org/kde/"
"krita/merge_requests для коду програми, https://invent.kde.org/websites/docs-"
"krita-org/merge_requests для вмісту підручника з Krita."

#: ../../contributors_manual/community.rst:78
msgid ""
"Host the code git repository: https://invent.kde.org/kde/krita . Note that "
"while there are mirrors of our git repository on Github and Phabricator, we "
"do not use them for Krita development."
msgstr ""
"Є сховищем коду у git: https://invent.kde.org/kde/krita . Зауважте, що хоча "
"існують дзеркала нашого сховища git на Github та Phabricator, ми не "
"використовуємо їх у процесі розробки Krita."

#: ../../contributors_manual/community.rst:79
msgid ""
"Host the Krita Manual content repository: https://invent.kde.org/websites/"
"docs-krita-org"
msgstr ""
"Містить сховище коду підручника з Krita: https://invent.kde.org/websites/"
"docs-krita-org"

#: ../../contributors_manual/community.rst:81
msgid "**Do not** make new issues on Gitlab or use it to make bug reports."
msgstr ""
"**Не** створюйте нові записи вад на Gitlab і не використовуйте його для "
"звітів щодо вад."

#: ../../contributors_manual/community.rst:83
msgid ""
"**Do** put all your code submissions (merge requests) on Gitlab. **Do not** "
"attach patches to bugs in the bug tracker."
msgstr ""
"**Розміщуйте** усі ваші зміни у коді (запити щодо злиття) на Gitlab. **Не "
"долучайте** латки до повідомлень щодо вад у системі стеження за вадами."

#: ../../contributors_manual/community.rst:88
msgid "Phabricator serves the following purposes for the Krita team:"
msgstr "Phabricator у команді Krita виконує такі ролі:"

#: ../../contributors_manual/community.rst:90
msgid ""
"Track what we are working on: https://phabricator.kde.org/maniphest/ This "
"includes development tasks, designing new features and UX design, as well as "
"tasks related to the website."
msgstr ""
"Список завдань, над якими ми працюємо: https://phabricator.kde.org/"
"maniphest/ У списку є завдання з розробки програмного коду, дизайну нових "
"можливостей та інтерфейсу користувача, а також завдання, які пов'язано із "
"сайтом програми."

#: ../../contributors_manual/community.rst:92
msgid ""
"**Do not** report bugs as tasks on Phabricator. Phabricator is where we "
"organize our work."
msgstr ""
"**Не повідомляйте** про вади за допомогою завдань (tasks) на Phabricator. "
"Phabricator призначено для упорядковування нашої роботи."

#: ../../contributors_manual/community.rst:95
msgid "Bugzilla: the Bug Tracker"
msgstr "Bugzilla: система стеження за вадами"

#: ../../contributors_manual/community.rst:97
msgid ""
"Krita shares the bug tracker with the rest of the KDE community. Krita bugs "
"are found under the Krita product. There are two kinds of reports in the bug "
"tracker: bugs and wishes. See the chapters on :ref:`Bug Reporting "
"<bugs_reporting>` and :ref:`Bug Triaging <triaging_bugs>` on how to handle "
"bugs. Wishes are feature requests. Do not report feature requests in "
"bugzilla unless a developer has asked you to. See the chapter on :ref:"
"`Feature Requests <developing_features>` for what is needed to create a good "
"feature request."
msgstr ""
"Krita використовує ту саму систему стеження за вадами, що і решта проєктів "
"спільноти KDE. Повідомлення про вади у Krita зібрано у категорії продукту "
"Krita. У системі стеження за вадами передбачено два типи звітів щодо вад: "
"звіти щодо недоліків у програмі та побажання. Ознайомтеся із розділами щодо :"
"ref:`звітування про вади <bugs_reporting>` та :ref:`пошуку причин вад "
"<triaging_bugs>`, щоб дізнатися про те, які працювати з вадами. Побажання є "
"запитами щодо реалізації можливостей. Не створюєте запитів щодо реалізації "
"можливостей у системі стеження за вадами, якщо про це вас не просили "
"розробники. Див. :ref:`розділ щодо запитів <developing_features>`, щоб "
"дізнатися про те, як створити якісний запит щодо реалізації нової можливості."

#: ../../contributors_manual/community.rst:100
msgid "Sprints"
msgstr "Спринти"

#: ../../contributors_manual/community.rst:102
msgid ""
"Sometimes, core Krita developers and users come together, most often in "
"Deventer, the Netherlands, to work together on our code design, UX design, "
"the website or whatever needs real, face-to-face contact. Travel to sprints "
"is usually funded by KDE e.V., while accommodation is funded by the Krita "
"Foundation."
msgstr ""
"Іноді основні розробники і користувачі Krita збираються разом, найчастіше у "
"Девентері у Нідерландах, для того, щоб попрацювати разом над програмним "
"кодом, дизайном інтерфейсу, сайтом та іншими нагальними питаннями, які "
"потребують особистого обговорення. Транспортні витрати, зазвичай, бере на "
"себе KDE e.V., а витрати на проживання — Krita Foundation."

#~ msgid "Review code submissions: https://phabricator.kde.org/differential/"
#~ msgstr ""
#~ "Подання латок до коду для рецензування: https://phabricator.kde.org/"
#~ "differential/"
