# translation of docs_krita_org_reference_manual___brushes___brush_engines___particle_brush_engine.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___particle_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 08:38+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Iterations"
msgstr "Iterácie"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:1
msgid "The Particle Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:16
msgid "Particle Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
#, fuzzy
#| msgid "Brush Size"
msgid "Brush Engine"
msgstr "Veľkosť štetca"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:19
msgid ".. image:: images/icons/particlebrush.svg"
msgstr ".. image:: images/icons/particlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:20
msgid ""
"A brush that draws wires using parameters. These wires always get more "
"random and crazy over drawing distance. Gives very intricate lines best used "
"for special effects."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:23
msgid "Options"
msgstr "Voľby"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:24
msgid ":ref:`option_size_particle`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:26
msgid ":ref:`option_opacity_n_flow`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:27
msgid ":ref:`option_airbrush`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:32
msgid "Brush Size"
msgstr "Veľkosť štetca"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:34
msgid "Particles"
msgstr "Častice"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:35
msgid "How many particles there's drawn."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:36
msgid "Opacity Weight"
msgstr "Váha nepriehľadnosti"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:37
msgid "The Opacity of all particles. Is influenced by the painting mode."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:38
msgid "Dx Scale (Distance X Scale)"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:39
msgid ""
"How much the horizontal cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:40
msgid "Dy Scale (Distance Y Scale)"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:41
msgid ""
"How much the vertical cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:42
msgid "Gravity"
msgstr "Gravitácia"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:43
msgid ""
"Multiplies with the previous particle's position, to find the new particle's "
"position."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:45
msgid ""
"The higher, the higher the internal acceleration is, with the furthest away "
"particle from the brush having the highest acceleration. This means that the "
"higher iteration is, the faster and more randomly a particle moves over "
"time, giving a messier result."
msgstr ""
