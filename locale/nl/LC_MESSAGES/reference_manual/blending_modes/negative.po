# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-21 12:54+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/blending_modes/negative.rst:1
msgid ""
"Page about the negative blending modes in Krita: Additive Subtractive, Arcus "
"Tangent, Difference, Equivalence, Exclusion, Negation."
msgstr ""
"Pagina over de negatieve mengmodi in Krita: Toegevoegde weggehaalde, "
"Arctangens, Verschil, Gelijkheid, Uitsluiting en Negatie."

#: ../../reference_manual/blending_modes/negative.rst:12
#: ../../reference_manual/blending_modes/negative.rst:16
msgid "Negative"
msgstr "Negatief"

#: ../../reference_manual/blending_modes/negative.rst:18
msgid "These are all blending modes which seem to make the image go negative."
msgstr ""
"Dit zijn alle mengmodi die het laten lijken dat de afbeelding negatief wordt."

#: ../../reference_manual/blending_modes/negative.rst:20
#: ../../reference_manual/blending_modes/negative.rst:24
msgid "Additive Subtractive"
msgstr "Toegevoegde weggehaalde"

#: ../../reference_manual/blending_modes/negative.rst:25
msgid "Subtract the square root of the lower layer from the upper layer."
msgstr ""
"De tweede machts wortel van de lagere laag aftrekken van de bovenste laag."

#: ../../reference_manual/blending_modes/negative.rst:30
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Additive_Subtractive_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Additive_Subtractive_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:30
msgid "Left: **Normal**. Right: **Additive Subtractive**."
msgstr "Links: **Normaal**. Rechts: **Toegevoegde weggehaalde**."

#: ../../reference_manual/blending_modes/negative.rst:32
#: ../../reference_manual/blending_modes/negative.rst:36
msgid "Arcus Tangent"
msgstr "Arctangens"

#: ../../reference_manual/blending_modes/negative.rst:38
msgid ""
"Divides the lower layer by the top. Then divides this by Pi. Then uses that "
"in an Arc tangent function, and multiplies it by two."
msgstr ""

#: ../../reference_manual/blending_modes/negative.rst:44
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Arcus_Tangent_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Arcus_Tangent_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:44
msgid "Left: **Normal**. Right: **Arcus Tangent**."
msgstr "Links: **Normaal**. Rechts: **Arctangens**."

#: ../../reference_manual/blending_modes/negative.rst:46
#: ../../reference_manual/blending_modes/negative.rst:50
msgid "Difference"
msgstr "Verschil"

#: ../../reference_manual/blending_modes/negative.rst:52
msgid ""
"Checks per pixel of which layer the pixel-value is highest/lowest, and then "
"subtracts the lower value from the higher-value."
msgstr ""

#: ../../reference_manual/blending_modes/negative.rst:58
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Difference_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Difference_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:58
msgid "Left: **Normal**. Right: **Difference**."
msgstr "Links: **Normaal**. Rechts: **Verschil**."

#: ../../reference_manual/blending_modes/negative.rst:60
#: ../../reference_manual/blending_modes/negative.rst:64
msgid "Equivalence"
msgstr "Gelijkheid"

#: ../../reference_manual/blending_modes/negative.rst:66
msgid ""
"Subtracts the underlying layer from the upper-layer. Then inverts that. "
"Seems to produce the same result as :ref:`bm_difference`."
msgstr ""

#: ../../reference_manual/blending_modes/negative.rst:72
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Equivalence_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Equivalence_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:72
msgid "Left: **Normal**. Right: **Equivalence**."
msgstr "Links: **Normaal**. Rechts: **Gelijkheid**."

#: ../../reference_manual/blending_modes/negative.rst:74
#: ../../reference_manual/blending_modes/negative.rst:78
msgid "Exclusion"
msgstr "Uitsluiting"

#: ../../reference_manual/blending_modes/negative.rst:80
msgid ""
"This multiplies the two layers, adds the source, and then subtracts the "
"multiple of two layers twice."
msgstr ""

#: ../../reference_manual/blending_modes/negative.rst:85
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Exclusion_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Exclusion_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:85
msgid "Left: **Normal**. Right: **Exclusion**."
msgstr "Links: **Normaal**. Rechts: **Uitsluiting**."

#: ../../reference_manual/blending_modes/negative.rst:87
#: ../../reference_manual/blending_modes/negative.rst:91
msgid "Negation"
msgstr "Negatie"

#: ../../reference_manual/blending_modes/negative.rst:93
msgid ""
"The absolute of the 1.0f value subtracted by base subtracted by the blend "
"layer. abs(1.0f - Base - Blend)"
msgstr ""

#: ../../reference_manual/blending_modes/negative.rst:98
msgid ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Negation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/negative/"
"Blending_modes_Negation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/negative.rst:98
msgid "Left: **Normal**. Right: **Negation**."
msgstr "Links: **Normaal**. Rechts: **Negatie**."
