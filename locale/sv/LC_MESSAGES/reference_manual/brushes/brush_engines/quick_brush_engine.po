# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:18+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:1
msgid "The Quick Brush Engine manual page."
msgstr "Manualsidan för snabbpenselgränssnittet."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:15
msgid "Quick Brush Engine"
msgstr "Snabbpenselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
msgid "Brush Engine"
msgstr "Penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:19
msgid ".. image:: images/icons/quickbrush.svg"
msgstr ".. image:: images/icons/quickbrush.svg"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:20
msgid ""
"A Brush Engine inspired by the common artist's workflow where a simple big "
"brush, like a marker, is used to fill large areas quickly, the Quick Brush "
"engine is an extremely simple, but quick brush, which can give the best "
"performance of all Brush Engines."
msgstr ""
"Ett penselgränssnitt inspirerat av det vanliga arbetsflödet för konstnärer "
"där en enkel stor pensel, som en markör, används för att fylla stora områden "
"snabbt. Snabbpenselgränssnittet är en extremt enkel men snabb pensel, som "
"kan ge bäst prestanda av alla penselgränssnitt."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:22
msgid ""
"It can only change size, blending mode and spacing, and this allows for "
"making big optimisations that aren't possible with other brush engines."
msgstr ""
"Den kan bara ändra storlek, blandningsläge och mellanrum, vilket tillåter "
"stora optimeringar som inte är möjliga med andra penselgränssnitt."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:26
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:27
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:30
msgid "Brush"
msgstr "Pensel"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:32
msgid "The only parameter specific to this brush."
msgstr "Den enda parameterns specifik för penseln."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:34
msgid "Diameter"
msgstr "Diameter"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:35
msgid ""
"The size. This brush engine can only make round dabs, but it can make them "
"really fast despite size."
msgstr ""
"Storleken. Penselgränssnittet kan bara göra runda klickar, men det kan göra "
"dem mycket snabbt trots storleken."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid "Spacing"
msgstr "Mellanrum"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid ""
"The spacing between the dabs. This brush engine is particular in that it's "
"faster with a lower spacing, unlike all other brush engines."
msgstr ""
"Mellanrummet mellan klickarna. Det här penselgränssnittet är speciellt "
"eftersom det är snabbare med mindre mellanrum, i motsats till alla andra "
"penselgränssnitt."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:40
msgid "`Phabricator Task <https://phabricator.kde.org/T3492>`_"
msgstr "`Phabricator uppgift <https://phabricator.kde.org/T3492>`_"
