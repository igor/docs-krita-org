# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:13+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/preferences/canvas_only_mode.rst:1
msgid "Canvas only mode settings in Krita."
msgstr "Inställningar för enbart duk i Krita."

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
#: ../../reference_manual/preferences/canvas_only_mode.rst:16
msgid "Canvas Only Mode"
msgstr "Enbart duk"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Preferences"
msgstr "Anpassning"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Settings"
msgstr "Inställningar"

#: ../../reference_manual/preferences/canvas_only_mode.rst:18
msgid ""
"Canvas Only mode is Krita's version of full screen mode. It is activated by "
"hitting the :kbd:`Tab` key on the keyboard. Select which parts of Krita will "
"be hidden in canvas-only mode -- The user can set which UI items will be "
"hidden in canvas-only mode. Selected items will be hidden."
msgstr ""
"Enbart duk är Kritas version av fullskärmsläge. Det aktiveras genom att "
"trycka på tangenten :kbd:`Tab` på tangentbordet. Välj vilka delar av Krita "
"som ska döljas med enbart duk: Användaren kan ställa in vilka objekt i "
"användargränssnittet som kan döljas med enbart duk. Markerade objekt kommer "
"att döljas."
