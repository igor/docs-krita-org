msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___blending_modes___mix.pot\n"

#: ../../reference_manual/blending_modes/mix.rst:None
msgid ".. image:: images/blending_modes/mix/Greaterblendmode.gif"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:1
msgid ""
"Page about the mix blending modes in Krita: Allanon, Alpha Darken, Behind, "
"Erase, Geometric Mean, Grain Extract, Grain Merge, Greater, Hard Mix, Hard "
"Overlay, Interpolation, Interpolation2x, Normal, Overlay, Parallel, Penumbra "
"A, B, C and D."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:16
msgid "Mix"
msgstr "混合"

#: ../../reference_manual/blending_modes/mix.rst:18
#: ../../reference_manual/blending_modes/mix.rst:22
msgid "Allanon"
msgstr "相加减半"

#: ../../reference_manual/blending_modes/mix.rst:24
msgid ""
"Blends the upper layer as half-transparent with the lower. (It add the two "
"layers together and then halves the value)."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:29
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Allanon_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:29
msgid "Left: **Normal**. Right: **Allanon**."
msgstr "左： **正常** ； 右： **相加减半** 。"

#: ../../reference_manual/blending_modes/mix.rst:31
#: ../../reference_manual/blending_modes/mix.rst:35
msgid "Interpolation"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:37
msgid ""
"Subtract 0.5f by 1/4 of cosine of base layer subtracted by 1/4 of cosine of "
"blend layer assuming 0-1 range. The result is similar to Allanon mode, but "
"with more contrast and functional difference to 50% opacity."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:43
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:43
msgid "Left: **Normal**. Right: **Interpolation**."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:45
msgid "Interpolation2x"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:49
msgid "Interpolation - 2X"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:51
msgid ""
"Applies Interpolation blend mode to base and blend layers, then duplicate to "
"repeat interpolation blending."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:56
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Interpolation_X2_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:56
msgid "Left: **Normal**. Right: **Interpolation - 2X**."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:58
#: ../../reference_manual/blending_modes/mix.rst:62
msgid "Alpha Darken"
msgstr "透明度变暗"

#: ../../reference_manual/blending_modes/mix.rst:64
msgid ""
"As far as I can tell this seems to premultiply the alpha, as is common in "
"some file-formats."
msgstr "貌似会对上下两个图层的颜色数值进行左乘，在某些文件格式里比较常见。"

#: ../../reference_manual/blending_modes/mix.rst:69
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Alpha_Darken_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:69
msgid "Left: **Normal**. Right: **Alpha Darken**."
msgstr "左： **正常** ； 右： **透明度变暗** 。"

#: ../../reference_manual/blending_modes/mix.rst:71
#: ../../reference_manual/blending_modes/mix.rst:75
msgid "Behind"
msgstr "背后"

#: ../../reference_manual/blending_modes/mix.rst:77
msgid ""
"Does the opposite of normal, and tries to have the upper layer rendered "
"below the lower layer."
msgstr "和 :ref:`bm_normal` 正好相反，把上面的图层在下面的图层背后进行绘制。"

#: ../../reference_manual/blending_modes/mix.rst:82
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Behind_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:82
msgid "Left: **Normal**. Right: **Behind**."
msgstr "左： **正常** ； 右： **背后** 。"

#: ../../reference_manual/blending_modes/mix.rst:84
msgid "Erase (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:88
msgid "Erase"
msgstr "清除"

#: ../../reference_manual/blending_modes/mix.rst:90
msgid ""
"This subtracts the opaque pixels of the upper layer from the lower layer, "
"effectively erasing."
msgstr "从下层的颜色中减去上层不透明的部分，等于用上层的不透明区域去擦除下层。"

#: ../../reference_manual/blending_modes/mix.rst:95
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Erase_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:95
msgid "Left: **Normal**. Right: **Erase**."
msgstr "左： **正常** ； 右： **清除** 。"

#: ../../reference_manual/blending_modes/mix.rst:97
#: ../../reference_manual/blending_modes/mix.rst:101
msgid "Geometric Mean"
msgstr "几何平均值"

#: ../../reference_manual/blending_modes/mix.rst:103
msgid ""
"This blending mode multiplies the top layer with the bottom, and then "
"outputs the square root of that."
msgstr "把上下两层颜色数值相乘然后在结果上开平方。"

#: ../../reference_manual/blending_modes/mix.rst:108
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Geometric_Mean_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:108
msgid "Left: **Normal**. Right: **Geometric Mean**."
msgstr "左： **正常** ； 右： **几何平均值** 。"

#: ../../reference_manual/blending_modes/mix.rst:110
#: ../../reference_manual/blending_modes/mix.rst:114
msgid "Grain Extract"
msgstr "颗粒抽取"

#: ../../reference_manual/blending_modes/mix.rst:116
msgid ""
"Similar to subtract, the colors of the upper layer are subtracted from the "
"colors of the lower layer, and then 50% gray is added."
msgstr ""
"和 :ref:`bm_subtract` 类似，从下层颜色数值中减去上层颜色数值，然后加入 50% 的"
"灰的颜色数值。"

#: ../../reference_manual/blending_modes/mix.rst:121
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Extract_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:121
msgid "Left: **Normal**. Right: **Grain Extract**."
msgstr "左： **正常** ； 右： **颗粒抽取** 。"

#: ../../reference_manual/blending_modes/mix.rst:123
#: ../../reference_manual/blending_modes/mix.rst:127
msgid "Grain Merge"
msgstr "颗粒合并"

#: ../../reference_manual/blending_modes/mix.rst:129
msgid ""
"Similar to addition, the colors of the upper layer are added to the colors, "
"and then 50% gray is subtracted."
msgstr ""
"和 :ref:`bm_addition` 类似，把上下层颜色数值相加，然后减去 50% 的灰的颜色数"
"值。"

#: ../../reference_manual/blending_modes/mix.rst:134
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Grain_Merge_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:134
msgid "Left: **Normal**. Right: **Grain Merge**."
msgstr "左： **正常** ； 右： **颗粒合并** 。"

#: ../../reference_manual/blending_modes/mix.rst:136
msgid "Greater (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:140
msgid "Greater"
msgstr "更不透明"

#: ../../reference_manual/blending_modes/mix.rst:142
msgid ""
"A blending mode which checks whether the painted color is painted with a "
"higher opacity than the existing colors. If so, it paints over them, if not, "
"it doesn't paint at all."
msgstr ""
"检查新绘制的颜色是否比画布上的已有颜色更不透明，如果是，那就把更不透明的颜色"
"画出来；如果否，则不画出任何颜色。"

#: ../../reference_manual/blending_modes/mix.rst:147
#: ../../reference_manual/blending_modes/mix.rst:151
msgid "Hard Mix"
msgstr "实色混合"

#: ../../reference_manual/blending_modes/mix.rst:153
msgid "Similar to Overlay."
msgstr "和 :ref:`bm_overlay` 类似。"

#: ../../reference_manual/blending_modes/mix.rst:155
msgid ""
"Mixes both Color Dodge and Burn blending modes. If the color of the upper "
"layer is darker than 50%, the blending mode will be Burn, if not the "
"blending mode will be Color Dodge."
msgstr ""
"组合使用 :ref:`bm_color_dodge` 和 :ref:`bm_color_burn` 。检查上层颜色的亮度是"
"否高于 0.5，如果是，则使用颜色减淡模式，如果否，则使用颜色加深模式。"

#: ../../reference_manual/blending_modes/mix.rst:161
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Mix_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:161
msgid "Left: **Normal**. Right: **Hard Mix**."
msgstr "左： **正常** ； 右： **实色混合** 。"

#: ../../reference_manual/blending_modes/mix.rst:166
msgid "Hard Mix (Photoshop)"
msgstr "实色混合 (Photoshop)"

#: ../../reference_manual/blending_modes/mix.rst:168
msgid "This is the hard mix blending mode as it is implemented in photoshop."
msgstr "和 Photoshop 中的实色混合模式完全相同。"

#: ../../reference_manual/blending_modes/mix.rst:174
msgid ".. image:: images/blending_modes/mix/Krita_4_0_hard_mix_ps.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:174
msgid ""
"**Left**: Dots are mixed in with the normal blending mode, on the **Right**: "
"Dots are mixed in with hardmix."
msgstr "左： **正常** ； 右： **实色混合 (Photoshop)** 。"

#: ../../reference_manual/blending_modes/mix.rst:176
msgid ""
"This add the two values, and then checks if the value is above the maximum. "
"If so it will output the maximum, otherwise the minimum."
msgstr ""
"把上下两层的颜色数值相加，如果数值超过最大值，则输出最大值；否则输出最小值。"

#: ../../reference_manual/blending_modes/mix.rst:178
msgid "Hard OVerlay"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:182
msgid "Hard Overlay"
msgstr "强叠加"

#: ../../reference_manual/blending_modes/mix.rst:186
msgid ""
"Similar to Hard light but hard light use Screen when the value is above 50%. "
"Divide gives better results than Screen, especially on floating point images."
msgstr ""
"和 :ref:`bm_hard_light` 类似，但用除去代替滤色。组合使用 :ref:`bm_divide` "
"和 :ref:`bm_multiply` 。检查上层颜色的亮度是否高于 0.5，如果是，则使用除去模"
"式，如果否，则使用相乘模式。除去的效果要好于滤色，尤其是在浮点图像中。"

#: ../../reference_manual/blending_modes/mix.rst:191
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Hard_Overlay_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:191
msgid "Left: **Normal**. Right: **Hard Overlay**."
msgstr "左： **正常** ； 右： **强叠加** 。"

#: ../../reference_manual/blending_modes/mix.rst:193
msgid "Normal (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:193
msgid "Source Over"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:197
msgid "Normal"
msgstr "正常"

#: ../../reference_manual/blending_modes/mix.rst:199
msgid ""
"As you may have guessed this is the default Blending mode for all layers."
msgstr "默认的图层混色模式。"

#: ../../reference_manual/blending_modes/mix.rst:201
msgid ""
"In this mode, the computer checks on the upper layer how transparent a pixel "
"is, which color it is, and then mixes the color of the upper layer with the "
"lower layer proportional to the transparency."
msgstr ""
"检查上面图层颜色数值的不透明度，然后按照不透明度的比例把上层颜色与下面图层的"
"颜色混合。"

#: ../../reference_manual/blending_modes/mix.rst:206
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Normal_50_Opacity_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:206
msgid "Left: **Normal** 100% Opacity. Right: **Normal** 50% Opacity."
msgstr "左： **正常** 100% 不透明度； 右： **正常** 50% 不透明度。"

#: ../../reference_manual/blending_modes/mix.rst:208
msgid "Overlay (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:212
msgid "Overlay"
msgstr "叠加"

#: ../../reference_manual/blending_modes/mix.rst:214
msgid ""
"A combination of the Multiply and Screen blending modes, switching between "
"both at a middle-lightness."
msgstr ""
"和 :ref:`bm_hard_light` 类似，是 :ref:`bm_multiply` 和 :ref:`bm_screen` 的组"
"合运用，两者的切换以中间亮度为界。"

#: ../../reference_manual/blending_modes/mix.rst:216
msgid ""
"Overlay checks if the color on the upperlayer has a lightness above 0.5. If "
"so, the pixel is blended like in Screen mode, if not the pixel is blended "
"like in Multiply mode."
msgstr ""
"叠加模式会检查上层颜色的亮度是否高于 0.5，如果是，则使用滤色模式，如果否，则"
"使用相乘模式。它的行为与强光模式相反。"

#: ../../reference_manual/blending_modes/mix.rst:218
msgid "This is useful for deepening shadows and highlights."
msgstr "常被用于加深阴影和加亮高光，提高图像反差。"

#: ../../reference_manual/blending_modes/mix.rst:223
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Overlay_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:223
msgid "Left: **Normal**. Right: **Overlay**."
msgstr "左： **正常** ； 右： **叠加** 。"

#: ../../reference_manual/blending_modes/mix.rst:225
#: ../../reference_manual/blending_modes/mix.rst:229
msgid "Parallel"
msgstr "平行"

#: ../../reference_manual/blending_modes/mix.rst:231
msgid ""
"This one first takes the percentage in two decimal behind the comma for both "
"layers. It then adds the two values. Divides 2 by the sum."
msgstr ""
"把上下两个图层的颜色数值按照各自数值小数点后两位为百分比取值，然后把得到的数"
"值相加后减半。"

#: ../../reference_manual/blending_modes/mix.rst:238
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Parallel_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:238
msgid "Left: **Normal**. Right: **Parallel**."
msgstr "左： **正常** ； 右： **平行** 。"

#: ../../reference_manual/blending_modes/mix.rst:240
#: ../../reference_manual/blending_modes/mix.rst:244
msgid "Penumbra A"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:246
msgid ""
"Creates a linear penumbra falloff. This means most tones will be in the "
"midtone ranges."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:251
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_A_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:251
msgid "Left: **Normal**. Right: **Penumbra A**."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:253
#: ../../reference_manual/blending_modes/mix.rst:257
msgid "Penumbra B"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:259
msgid "Penumbra A with source and destination layer swapped."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:264
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_B_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:264
msgid "Left: **Normal**. Right: **Penumbra B**."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:266
#: ../../reference_manual/blending_modes/mix.rst:270
msgid "Penumbra C"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:272
msgid ""
"Creates a penumbra-like falloff using arc-tangent formula. This means most "
"tones will be in the midtone ranges."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:277
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_C_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:277
msgid "Left: **Normal**. Right: **Penumbra C**."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:279
#: ../../reference_manual/blending_modes/mix.rst:283
msgid "Penumbra D"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:285
msgid "Penumbra C with source and destination layer swapped."
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:290
msgid ""
".. image:: images/blending_modes/mix/"
"Blending_modes_Penumbra_D_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/mix.rst:290
msgid "Left: **Normal**. Right: **Penumbra D**."
msgstr ""
