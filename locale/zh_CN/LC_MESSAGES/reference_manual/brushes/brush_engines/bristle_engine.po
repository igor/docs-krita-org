msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___bristle_engine."
"pot\n"

#: ../../<generated>:1
msgid "Weighted saturation"
msgstr "饱和度权衡"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:1
msgid "The Bristle Brush Engine manual page."
msgstr "介绍 Krita 的鬃毛笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:18
msgid "Bristle Brush Engine"
msgstr "鬃毛笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Hairy Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Sumi-e"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:21
msgid ".. image:: images/icons/bristlebrush.svg"
msgstr ".. image:: images/icons/bristlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:22
msgid ""
"A brush intended to mimic real-life brushes by drawing the trails of their "
"lines or bristles."
msgstr "这是一个模拟了现实笔刷效果的引擎，它可以在描绘路线上按照鬃毛留下痕迹。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:25
msgid "Brush Tip"
msgstr "笔尖"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:27
msgid "Simply put:"
msgstr "基本原理："

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:29
msgid "The brush tip defines the areas with bristles in them."
msgstr "笔尖定义了笔刷含有鬃毛的区域。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:30
msgid ""
"Lower opacity areas have lower-opacity bristles. With this brush, this may "
"give the illusion that lower-opacity areas have fewer bristles."
msgstr ""
"在不透明度较低的区域里的鬃毛的不透明度跟着降低。在这个笔刷引擎里面，不透明度"
"较低的区域可能给人留下鬃毛较少的印象。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:31
msgid ""
"The :ref:`option_size` and :ref:`option_rotation` dynamics affect the brush "
"tip, not the bristles."
msgstr ""
":ref:`option_size` 和 :ref:`option_rotation` 选项影响笔尖整体效果，但不控制鬃"
"毛本身的大小和旋转。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:33
msgid "You can:"
msgstr "使用技巧："

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:35
msgid ""
"Use different shapes for different effects. Be aware that complex brush "
"shapes will draw more slowly though, while the effects aren't always visible "
"(since in the end, you're passing over an area with a certain number of "
"bristles)."
msgstr ""
"你可以使用不同的笔尖形状来打造不同的效果，但笔刷形状越复杂，绘制速度就越慢，"
"且效果不一定明显，因为一根鬃毛很容易就画到了别的鬃毛已经画过的位置，形成丰满"
"的笔迹。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:36
msgid ""
"To decrease bristle density, you can also just use an autobrush and decrease "
"the brush tip's density, or increase its randomness."
msgstr ""
"要减小鬃毛的密度，可以使用自动笔尖并降低鬃毛选项的密度，或者增加随机偏移量等"
"方法实现。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:39
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:41
msgid "Bristle Options"
msgstr "鬃毛选项"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:43
msgid "The core of this particular brush-engine."
msgstr "此选项页是鬃毛笔刷引擎的核心功能。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:46
msgid ""
"Think of it as pressing down on a brush to make the bristles further apart."
msgstr "控制用力按下笔刷时鬃毛分散的程度。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:48
msgid ""
"Larger values basically give you larger brushes and larger bristle spacing. "
"For example, a value of 4 will multiply your base brush size by 4, but the "
"bristles will be 4 times more spaced apart."
msgstr ""
"此数值越大，笔画宽度就越大，且鬃毛之间的间距也就越远。例如，此数值为 4 时会将"
"你的笔刷大小增大 4 倍，而鬃毛之间的距离也会增大 4 倍。整个笔画的轮廓就会显得"
"稀疏起来。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:49
msgid ""
"Use smaller values if you want a \"dense\" brush, i.e. you don't want to see "
"so many bristles within the center."
msgstr "此数值越小，笔刷鬃毛越凝聚，这样笔画中心部分就不会出现稀疏的情况了。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid "Scale"
msgstr "缩放"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid ""
"Negative values have the same effect as corresponding positive values: -1.00 "
"will look like 1.00, etc."
msgstr "负值的作用和对应的正值相同。例如 -1.00 的效果和 1.00 是一样的。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:53
msgid "Adds a jaggy look to the trailing lines."
msgstr "给鬃毛的痕迹添加一点抖动感。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:55
msgid "At 0.00, all the bristles basically remain completely parallel."
msgstr "设为 0.00 时鬃毛会保持平行。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid "Random Offset"
msgstr "随机偏移"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid ""
"At other values, the bristles are offset randomly. Large values will "
"increase the brush size a bit because of the bristles spreading around, but "
"not by much."
msgstr ""
"设为其他数值时鬃毛的痕迹会进行随机偏移。数值较大时鬃毛会散开，所以会略微增大"
"笔刷的整体大小。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:57
msgid "Negative values have the same effect as corresponding positive values."
msgstr "负值的效果和正值相同。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:58
msgid "Shear"
msgstr "切变"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:59
msgid ""
"Shear introduces an angle to your brush, as though you're drawing with an "
"oval brush (or the side of a round brush)."
msgstr ""
"此数值会对笔刷的整体轮廓进行切变，把本来圆头的笔刷变成扁头，或者模仿使用圆头"
"的一侧描绘的效果。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid "Density"
msgstr "密度"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid ""
"This controls the density of bristles. Scale takes a number of bristles and "
"expands or compresses them into a denser area, whereas density takes a fixed "
"area and determines the number of bristles in it. See the difference?"
msgstr ""
"控制鬃毛的密度。前面的缩放选项会把鬃毛集约或者分散到一定的区域内，而密度则决"
"定某个固定区域内的鬃毛的多少。下图从上到下依次展示了缩放、随机偏移、切变、密"
"度在不同数值下的效果。现在心里有数了吧？"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:64
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:66
msgid ""
"This one maps \"Scale\" to mouse speed, thus simulating pressure with a "
"graphics tablet!"
msgstr "此选项会把“缩放”数值映射到鼠标速度，从而模拟数位板的压力。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid "Mouse Pressure"
msgstr "鼠标压力"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid ""
"Rather, it uses the \"distance between two events\" to determine scale. "
"Faster drawing, larger distances."
msgstr ""
"此选项使用“两次鼠标事件之间的距离”来决定缩放，画得越快，距离越大。下图上方的"
"例子是鼠标压力选项的效果，图中起始处压力全开的问题貌似已经在新版中解决。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:69
msgid ""
"This doesn't influence the \"pressure\" input for anything else (size, "
"opacity, rotation etc.) so you still have to map those independently to "
"something else."
msgstr "此选项不会影响“压力”传感器对其他选项的影响，如大小、不透明度、旋转等。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:70
msgid "Threshold"
msgstr "阈值"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:71
msgid ""
"This is a tablet feature. When you turn this on, only bristles that are able "
"to \"touch the canvas\" will be painted."
msgstr ""
"这是一个转为数位板而设的选项。勾选后，鬃毛在只有压力超过一定数值后才会被软件"
"认为“接触了画布”而画出笔迹。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:72
msgid "Connect Hairs"
msgstr "鬃毛粘连"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:73
msgid "The bristles get connected. See for yourself."
msgstr "鬃毛的笔迹会互相粘连到一起，放大笔画后容易看清效果。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:74
msgid "Anti-Aliasing"
msgstr "抗锯齿"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:75
msgid "This will decrease the jaggy-ness of the lines."
msgstr "此选项会降低笔画轮廓的不规则感，缩小笔刷或者视图后容易看清效果。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid "Composite Bristles"
msgstr "鬃毛混色"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid ""
"This \"composes the bristle colors within one dab,\" but explains that the "
"effect is \"probably subtle\"."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:80
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:82
msgid "Ink Depletion"
msgstr "颜料消耗"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:84
msgid ""
"This simulated ink depletion over drawing time. The value dictates how long "
"it will take. The curve dictates the speed."
msgstr ""
"根据每笔的持续时间来模拟颜料的消耗情况。颜料量控制颜料需要多久耗尽，曲线控制"
"在每笔的不同部分中的颜料比例。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:86
msgid "Opacity"
msgstr "不透明度"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:87
msgid "The brush will go transparent to simulate ink-depletion."
msgstr "颜料耗尽时笔画会变成透明。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:89
msgid "The brush will be desaturated to simulate ink-depletion."
msgstr "颜料耗尽时笔画会失去饱和度。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:91
msgid "Saturation"
msgstr "饱和度"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:94
msgid ""
"The brush will pick up colors from other brushes. You don't need to have :"
"guilabel:`Ink depletion` checked to activate this option, you just have to "
"check :guilabel:`Soak ink`. What this does is cause the bristles of the "
"brush to take on the colors of the first area they touch. Since the Bristle "
"brush is made up of independent bristles, you can basically take on several "
"colors at the same time."
msgstr ""
"勾选此项后，笔刷的鬃毛将拾取笔画起点的画布已有颜色进行绘制。笔刷的不同鬃毛可"
"以拾取到不同颜色并在同一笔画里绘制出来。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:98
msgid ""
"It will only take colors in the unscaled area of the brush, so if you're "
"using a brush with 4.00 scale for example, it will only take the colors in "
"the 1/4 area closest to the center."
msgstr ""
"此功能只会拾取笔刷未经缩放的区域。如果你的笔刷的缩放比例为 4.00，那么它只会拾"
"取笔画中央 1/4 区域的颜色。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:99
msgid "When the source is transparent, the bristles take black color."
msgstr "如果汲取的区域为透明，鬃毛将拾取到黑色。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid "Soak Ink"
msgstr "汲取颜色"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid ""
"Be aware that this feature is a bit buggy though. It's supposed to take the "
"color from the current layer, but some buggy behavior causes it to often use "
"the last layer you've painted on (with a non-Bristle brush?) as source. To "
"avoid these weird behaviors, stick to just one layer, or paint something on "
"the current active layer first with another brush (such as a Pixel brush)."
msgstr ""
"此功能可能并不太稳定。它本应拾取当前图层的颜色，但有时候会拾取到上一个你用非"
"鬃毛笔刷绘制过的图层。如果发生此类情况，可以试试一直在同一图层作画，或者在当"
"前图层中用别的笔刷绘制一下，然后切换到鬃毛笔刷作画。"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:107
msgid "Works by modifying the saturation with the following:"
msgstr "此功能通过权重下列数值来控制饱和度："

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:109
msgid "Pressure weight"
msgstr "压力权重"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:110
msgid "Bristle length weight"
msgstr "鬃毛长度权重"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:111
msgid "Bristle ink amount weight"
msgstr "鬃毛颜料量权重"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:112
msgid "Ink depletion curve weight"
msgstr "颜料消耗曲线权重"
